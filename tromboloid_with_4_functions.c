//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>

int input()
{
    float a;
    printf("Enter the height,breadth and depth: ");
    scanf("%f",&a);
    return a;
}

float volume(float a,float b,float c)
{
    float volume;
    volume = ((a*b*c)+(b/c))*1/3;
    return volume;
}

float output(float a,float b,float c,float d)
{
    printf("The volume of tromboloid with height-%f,depth-%f and breadth-%f is %f",a,b,c,d);
}

int main()
{
    float h,d,b,v;
    h=input();
    d=input();
    b=input();
    v=volume(h,d,b);
    output(h,d,b,v);
    return 0;
}
    
