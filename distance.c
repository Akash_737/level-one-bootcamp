//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include<math.h>

int
input ()
{
  float a;
  printf ("Enter the x and y coordinates of point 1 and point2\n");
  scanf ("%f", &a);
  return a;
}

int
find_distance (float x1, float x2, float y1, float y2)
{
  float distance;
  distance = sqrt ((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
  return distance;
}

void
output (float z, float x1, float y1, float x2, float y2)
{
  printf ("The distance between the (%f,%f) and (%f,%f) is %f", x1, y1, x2,
	  y2, z);
}

int
main ()
{
  float a1, a2, b1, b2, d;
  a1 = input ();
  b1 = input ();
  a2 = input ();
  b2 = input ();
  d = find_distance (a1, a2, b1, b2);
  output (d, a1, b1, a2, b2);
  return 0;
}
